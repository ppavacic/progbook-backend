#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_json;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate mysql;
extern crate rocket_cors;
extern crate rocket_contrib;
extern crate blob;
extern crate rand;
extern crate base64;

mod get;
mod common;
mod post;

use rocket_cors::Cors;

fn main() {
    let options = Cors::default();
    let pool = mysql::Pool::new("mysql://progbook-admin:progbook-admin@localhost:3306/progbook").expect("Can't connect to database");
 
    rocket::ignite().mount("/", routes![
    									get::user::user_profile::user_posts,
                                        get::user::user_info::user_info,
                                        post::user::follow::follow_user,
                                        post::user::follow::unfollow_user,
                                        post::user::user_info::update_info,
    									get::search::users_list, 
    									get::home::home_posts,
                                        post::post::post::new_post,
                                        post::post::favorite::favorite_post,
    									post::post::favorite::unfavorite_post,
    									get::conversation::conversations::conversations_list,
                                        get::conversation::contact::contact_person,
                                        get::conversation::conversation::messages_list,
                                        post::message::new_message,
                                        post::new_conversation::new_message,
    									get::sidebar::favorites_repos,
    									get::sidebar::popular_repos,
                                        post::register::register,
                                        post::login::login,
                                        post::repo::follow::follow_repo,
                                        post::repo::follow::unfollow_repo,
                                        post::repo::favorite::favorite_repo,
                                        get::repo::favorites::favorites,    
                                        get::repo::following::following,
                                        get::user::user_lists::get_user_info_gitlab_key
    									])
                    .attach(options)
                    .manage(pool)
    				.launch();
}
