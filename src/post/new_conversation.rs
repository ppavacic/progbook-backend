	use rocket::{Response, State};
use crate::common::{response, new_message};
use crate::common::user_token::UserToken;
use rocket::http::{RawStr};

#[post("/contact/<interl_id>", format = "text/plain", data = "<msg_txt>")]
pub fn new_message<'r>(user_token: UserToken, interl_id: &RawStr, state: State<'r, mysql::Pool>, msg_txt: String) -> Response<'static> {
	let pool = state.inner();
	let usr_id = user_token.identify().unwrap();
	let interl_id: i32 = interl_id.to_string().parse().expect("Interl ID not number");
	let query = "SELECT conv_id FROM conversation 
					WHERE (usr1_id = (:usr_id) AND usr2_id = (:interl_id)) 
					OR (usr1_id = (:interl_id) AND usr2_id = (:usr_id));".to_string();
	match pool.prep_exec(query, params!{usr_id, interl_id}) 
	.expect("new_message_auth: Something wrong with input query string").next() {
		Some(data) => { //if we already have conversation between those two people
			let conv_id: i32 = mysql::from_row(data.expect("Database error"));
			new_message::new_message(pool, conv_id, usr_id, msg_txt).unwrap();
			let json = json!({
				"conv_id": conv_id,
			});
			return response::build_success_json(json);
		},
		None => { // if we dont have conversation that exists already
			let query = "CALL CreateConversation(NULL, (:usr_id), (:interl_id))".to_string();
			let mysql_row = pool.prep_exec(query, params!(usr_id, interl_id))
								.expect("error creating conversation:\n").next().unwrap();
			let conv_id: i32 = mysql::from_row(mysql_row.unwrap());
			new_message::new_message(pool, conv_id, usr_id, msg_txt).unwrap();
			let json = json!({
				"conv_id": conv_id,
			});
			return response::build_success_json(json);
		}
	}


}
	