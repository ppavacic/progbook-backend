use crate::common::response::build_success_generic;
use rocket::{Response, State};
use crate::common::user_token::UserToken;

#[post("/follow/repo/check/<repo_id>")]
pub fn follow_repo<'r>(user_token: UserToken, repo_id: i32, state: State<'r, mysql::Pool>) -> Response<'static> {
	let pool = state.inner();
	let usr_id = user_token.identify().unwrap();

	let query = "SELECT * FROM repo_follow WHERE repo_id = (:repo_id) AND usr_id = (:usr_id);".to_string();
	if pool.prep_exec(query, params!{usr_id, repo_id})
			.expect("follow_user: Something wrong with input query string, check")
			.next().is_none() {
		let query = "INSERT INTO repo_follow VALUES ((:usr_id), (:repo_id));".to_string();
		pool.prep_exec(query, params!{usr_id, repo_id}).expect("follow_user: Something wrong with input query string, insert");
		return build_success_generic("Success, now following".to_string())
	}
	
	build_success_generic("Already following".to_string())
}