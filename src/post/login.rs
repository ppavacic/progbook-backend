use crate::common::{response, helper};
use rocket::response::Response;
use rocket_contrib::json::Json;
use rocket::http::Status;
use rocket::State;
use serde_json::Value;
use rand::prelude::*;
use rand::distributions::{Alphanumeric};
use mysql;

#[derive(Deserialize)]
pub struct LoginData {
	email: String,
	password: String
}

fn build_login(usr_id: i32, pool: &mysql::Pool) -> Result<Value, Response<'static>> {
	let user_token = thread_rng().sample_iter(&Alphanumeric).take(64).collect::<String>();
	let query = "INSERT INTO login(user_token, usr_id) VALUES((:user_token), (:usr_id));".to_string();

	match pool.prep_exec(query, params!{"user_token" => user_token.to_owned(), "usr_id" => usr_id}) {
		Ok(mut result) => {
			match result.next() {
				Some(result) => {
					//I believe this cant happen
					println!("⁉️ build_login2: \t{}", mysql::from_row::<String>(result.expect("⁉️ build_login error unpacking row")));
					return Err(response::build_failure_generic("Unhandled error", Status::InternalServerError));
				},
				None => {
					//this is what happens if user was created
					return Ok(json!({
						"user_token": user_token
					}));
				}
			}
		},
		Err(err) => {
			println!("⁉️ build_login1:\t{}", err);
		}
	}
	Err(response::build_failure_generic("Internal error", Status::InternalServerError))
}

#[post("/login", format="application/json", data="<data>")]
pub fn login<'a, 'r>(data: 	Json<LoginData>, state: State<'r, mysql::Pool>) -> Response<'a> {
	let data = data.into_inner();
	let query = "SELECT usr_id, password from user WHERE email=(:email);".to_string();
	let pool = state.inner();
	//hadnling request for email
	match pool.prep_exec(query, params!{"email" => data.email.to_owned()}) {
		//if email is found
		Ok(mut result) => {
			match result.next() {
				Some(result) => {
					let (usr_id, password): (i32, String) = mysql::from_row(result.expect("⁉️ login\t Error unpacking row"));
					//correct password
					if password == helper::hash_string(data.password).encode_base64() {
						match build_login(usr_id, pool) {
							Ok(json) => { return response::build_success_json(json); }
							Err(err_response) => { return err_response; }
						};
						
					} else {
						//wrong password entered
						return response::build_failure_generic("Invalid e-mail or password", Status::Unauthorized);
					}
					
				},
				//no e-mail in database, returning unauthorized because of attacks
				None => {
					return response::build_failure_generic("Invalid e-mail or password", Status::Unauthorized);
				},
			}
			
		},
		Err(err) => {
			println!("⁉️ login {}", err);
			return response::build_failure_generic("Unhandled error", Status::InternalServerError);
		}
	}
	
}