use rocket::{Response, State};
use crate::common::response;
use crate::common::user_token::UserToken;
use rocket::http::{Status};

#[post("/conversation/<conv_id>", format = "text/plain", data = "<msg_txt>")]
pub fn new_message<'r>(user_token: UserToken, conv_id: i32, msg_txt: String, state: State<'r, mysql::Pool>) -> Response<'static> {
	let pool = state.inner();
	let usr_id = user_token.identify().unwrap();
	let query = "SELECT
				CASE
					WHEN usr1_id = (:usr_id) OR usr2_id = (:usr_id) THEN true
					ELSE false
				END AS authorized
				FROM conversation
				WHERE conv_id = (:conv_id);".to_string();
	match pool.prep_exec(query, params!{usr_id, conv_id}) 
	.expect("new_message_auth: Something wrong with input query string").next() {
		Some(data) => {
			let auth: bool = mysql::from_row(data.expect("Database error"));
			if !auth { return response::build_failure_generic("Unauthorized", Status::Unauthorized) }
		},
		None => {
		}
	}

	let query = r#"CALL CreateMessage( (:conv_id), (:usr_id), (:msg_txt) )"#.to_string();
	let msg_txt_cpy = msg_txt.to_owned();

	let row = pool.prep_exec(query, params!{conv_id, usr_id, msg_txt})
	.expect("new_message_input: Something wrong with input query string").next()
	.expect("new_message_input: error writing to database");
	let data: mysql::Value = mysql::from_row(row.expect("unable to read row"));
	let mut create_date = data.as_sql(false);
	create_date.remove(0);
	create_date.remove(create_date.len() -1); //removing ' and ';

	let json = json!({
		"from_id": usr_id,
		"content_txt": msg_txt_cpy,
		"create_date": create_date
	});
	return response::build_success_json(json);
}
	