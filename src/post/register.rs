use rocket_contrib::json::Json;
use rocket::response::Response;
use rocket::http::{Status};
use crate::common::{helper, response};
use rocket::State;



#[derive(Deserialize)]
pub struct RegisterForm {
	email: String,
	password: String,
	display_name: String,
	birth_date: String
}


#[post("/register", format = "application/json", data="<data>")]
pub fn register<'a, 'r>(data: Json<RegisterForm>, state: State<'r, mysql::Pool>) -> Response<'a> {
	let data = data.into_inner();
	let password_blob = helper::hash_string(data.password);

	let pool = state.inner();
	let query = "INSERT INTO user VALUES(NULL, (:email), (:password), (:display_name), (:birth_date), NULL, NULL, NULL, NULL);".to_string();

	//TODO: handling of incorrect expressions
	let database_entry = pool.prep_exec(query, params!{
		"email" => data.email.to_owned().to_lowercase(),
		"password" => password_blob.encode_base64(),
		"display_name" => data.display_name.to_owned(),
		"birth_date" => data.birth_date.to_owned(),
	});

	//.expect("Email in use").next().expect("Err 2").expect("Err 3");
	match database_entry {
		Ok(_result) => {
			return response::build_success_generic("User succesfully created!".to_string());
		},
		Err(err)  => {
			println!("{}", err);
			return response::build_failure_generic("E-mail address already in use!", Status::Conflict);
		}
	}	
}