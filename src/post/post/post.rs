//INSERT INTO post(usr_id, content_txt) VALUES(8, "first post");
use rocket::Response;
use crate::common::user_token::UserToken;
use crate::common::response::build_success_generic;
use rocket::State;


#[post("/post", data="<content_txt>", format="text/plain")]
pub fn new_post<'r, 'a>(user_token: UserToken, content_txt: String, state: State<'r, mysql::Pool>) -> Response<'a>{
	let pool = state.inner();
	let query = "INSERT INTO post(usr_id, content_txt) VALUES ((:usr_id), (:content_txt));";
	let usr_id = user_token.identify().expect("no user");

	match pool.prep_exec(query, params!{usr_id, content_txt})
	.expect("new_message_input: Something wrong with input query string").next() {
		Some(data) => {
			println!("something: \n{:?}\n", data);
			return build_success_generic("Success".to_owned());
		},
		None => {
			println!("empty set");
			return build_success_generic("Success".to_owned());
		}
	}
}


