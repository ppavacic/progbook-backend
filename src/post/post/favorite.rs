use rocket::Response;
use crate::common::user_token::UserToken;
use crate::common::response::build_success_generic;
use rocket::State;


#[post("/post/favorite/<post_id>")]
pub fn favorite_post<'r, 'a>(user_token: UserToken, post_id: i32, state: State<'r, mysql::Pool>) -> Response<'a>{
	let pool = state.inner();
	let usr_id = user_token.identify().expect("no user");

	let query = "SELECT * FROM post_fav WHERE post_id = (:post_id) AND usr_id = (:usr_id)	;".to_string();
	if pool.prep_exec(query, params!{usr_id, post_id})
			.expect("favorite_post: Something wrong with input query string, check")
			.next().is_none() {
		let query = "INSERT INTO post_fav VALUES(:usr_id, :post_id);".to_string();
		pool.prep_exec(query, params!{usr_id, post_id}).expect("favorite_post: Something wrong with input query string, insert");
		return build_success_generic("Success, now favorited".to_string())
	}
	
	build_success_generic("Already favorited".to_string())
}

#[post("/post/unfavorite/<post_id>")]
pub fn unfavorite_post<'r, 'a>(user_token: UserToken, post_id: i32, state: State<'r, mysql::Pool>) -> Response<'a>{
	let pool = state.inner();
	let usr_id = user_token.identify().expect("no user");

	let query = "SELECT * FROM post_fav WHERE post_id = (:post_id) AND usr_id = (:usr_id);".to_string();
	if pool.prep_exec(query, params!{usr_id, post_id})
			.expect("unfavorite_post: Something wrong with input query string, check")
			.next().is_some() {
		let query = "DELETE FROM post_fav WHERE post_id = (:post_id) AND usr_id = (:usr_id);".to_string();
		pool.prep_exec(query, params!{usr_id, post_id}).expect("unfavorite_post: Something wrong with input query string, insert");
		return build_success_generic("Success, now unfavorited".to_string())
	}
	
	build_success_generic("Not favorited, so not unfavorited".to_string())
}
