use rocket_contrib::json::Json;
use crate::common::response::build_success_generic;
use rocket::{Response, State};
use crate::common::user_token::UserToken;

#[derive(Deserialize)]
pub struct NewInfo {
	name: Option<String>,
	key: Option<String>,
	change_key: bool,
	quote: Option<String>,
	gitlab_id: Option<i32>
}

#[post("/user/updateInfo", format="application/json", data="<new_info>")]
pub fn update_info<'r>(user_token: UserToken, new_info: Json<NewInfo>, state: State<'r, mysql::Pool>) -> Response<'static> {
	let pool = state.inner();
	let usr_id = user_token.identify().unwrap();

	if new_info.change_key {
		let query = "
		UPDATE user
			SET display_name = (:name), gitlab_API_key = (:key), personal_quote = (:quote), gitlab_id = (:gitlab_id)
		WHERE usr_id = (:usr_id)".to_string();
		let NewInfo {name, key, quote, gitlab_id, ..} = new_info.into_inner();
		pool.prep_exec(query, params!{name, key, quote, usr_id, gitlab_id}).expect("update_info: Error with query changekey=1");
	} else {
		let query = "
		UPDATE user
			SET display_name = (:name), personal_quote = (:quote) 
		WHERE usr_id = (:usr_id)".to_string();
		let NewInfo {name, quote, ..} = new_info.into_inner();
		pool.prep_exec(query, params!{name, quote, usr_id}).expect("Error with query changekey=0");
	}

	
	build_success_generic("Info update_info(user_token: UserToken, new_info: Json<NewInfo>, state: State<'r, mysql::Pool>)".to_string())
}