use crate::common::response::build_success_generic;
use rocket::{Response, State};
use crate::common::user_token::UserToken;

#[post("/user/follow/<subject_id>")]
pub fn follow_user<'r>(user_token: UserToken, subject_id: i32, state: State<'r, mysql::Pool>) -> Response<'static> {
	let pool = state.inner();
	let usr_id = user_token.identify().unwrap();

	let query = "SELECT * FROM user_follow WHERE stalker_id = (:usr_id) AND subject_id = (:subject_id);".to_string();
	if pool.prep_exec(query, params!{usr_id, subject_id})
			.expect("follow_user: Something wrong with input query string, check")
			.next().is_none() {
		let query = "INSERT INTO user_follow VALUES ((:usr_id), (:subject_id));".to_string();
		pool.prep_exec(query, params!{usr_id, subject_id}).expect("follow_user: Something wrong with input query string, insert");
		return build_success_generic("Success, now following".to_string())
	}
	
	build_success_generic("Already following".to_string())
}

#[post("/user/unfollow/<subject_id>")]
pub fn unfollow_user<'r>(user_token: UserToken, subject_id: i32, state: State<'r, mysql::Pool>) -> Response<'static> {
	let pool = state.inner();
	let usr_id = user_token.identify().unwrap();

	let query = "SELECT * FROM user_follow WHERE stalker_id =(:usr_id) AND subject_id = (:subject_id);".to_string();
	if pool.prep_exec(query, params!{usr_id, subject_id})
			.expect("follow_user: Something wrong with input query string")
			.next().is_some() {
		let query = "DELETE FROM user_follow WHERE stalker_id = (:usr_id) AND subject_id = (:subject_id);".to_string();
		pool.prep_exec(query, params!{usr_id, subject_id}).expect("follow_user: Something wrong with input query string, insert");
		return build_success_generic("Success, unfollowed".to_string());
	}


	build_success_generic("Wasn't following".to_string())
}
	