use rocket::State;
use rocket::response::Response;
use crate::common::response::{build_success_json};
use crate::common::user_token::UserToken;

#[get("/repo/following")]
pub fn following<'a, 'r>(user_token: UserToken, state: State<'r, mysql::Pool>) -> Response<'a> {		
	let usr_id = user_token.identify().unwrap();
	let pool = state.inner();
	let query = "SELECT repo_id FROM repo_follow WHERE usr_id = (:usr_id);".to_string();
	let mysql_row = pool.prep_exec(query, params!(usr_id)).expect("id_and_img: error with query");
	let mut results: Vec<i32> = Vec::new();
	for row in mysql_row {
		let repo_id: i32 = mysql::from_row(row.expect("error reading row"));
		results.push(repo_id)
	}
	let json = json!(results);
																																				
	build_success_json(json)
}