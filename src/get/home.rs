use rocket::State;
use rocket::response::Response;
use crate::common::{response, user_token};


#[get("/home")]
pub fn home_posts<'r>(user_token: user_token::UserToken, state: State<'r, mysql::Pool>) -> Response<'static> {	
	let usr_id = user_token.identify().unwrap();
	let pool = state.inner();
	let query = "
	SELECT user.usr_id, display_name, img_ref, post.post_id, post.content_txt, 
	post.create_date, COUNT(post_fav.usr_id), 
	CASE
		WHEN (SELECT COUNT(*) FROM post_fav WHERE usr_id = (:usr_id) AND post_fav.post_id = post.post_id) = 1 THEN true 
		ELSE false
	END as favorited
		FROM user_follow 
		JOIN post ON subject_id = usr_id 
		LEFT JOIN post_fav ON post.post_id = post_fav.post_id 
		JOIN user ON subject_id = user.usr_id 
	WHERE stalker_id = (:usr_id)
	GROUP BY post.post_id
	ORDER BY create_date DESC
	LIMIT 40;
	".to_string();

	let result = pool.prep_exec(query, params!(usr_id)).expect("home_posts: Error with input query");
	let mut json: Vec<serde_json::Value> = Vec::new();
	for row in result {
		let (usr_id, usr_name, usr_img, post_id, post_content, post_date, post_fav_cnt, post_fav) : 
			(i32, Option<String>, Option<String>, i32, String, mysql::Value, i32, bool) = 
			mysql::from_row(row.expect("home_posts: unable to read row"));
		let post_date = post_date.as_sql(false);
		json.push(json!({
			"usr": {
				"id": usr_id,
				"name": usr_name,
				"img": usr_img,
			},
			"post": {
				"id": post_id,
				"content": post_content,
				"date": post_date,
				"favorites": post_fav_cnt,
				"favorited": post_fav
			}
		}));
	}
																																				
	response::build_success_json(json!(json))
}