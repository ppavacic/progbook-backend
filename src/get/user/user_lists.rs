use rocket_contrib::json::Json;
use rocket::Response;
use crate::common::user_token::UserToken;
use rocket::State;
use crate::common::response::build_success_generic;

/*#[derive(Deserialize, Debug)]
struct Repo {
	problems: Vec<i32>
}*/
/*#[derive(Deserialize, Debug)]
pub struct Repos {
	repos: Vec<i32>
}*/
#[derive(Deserialize, Debug)]
pub struct Repos {
	repos: Vec<i32>
}


#[post("/users/gitlabkey", format="application/json", data="<repo_user>")]
pub fn get_user_info_gitlab_key<'r, 'a>(_user_token: UserToken, repo_user: Json<Vec<i32>>, _state: State<'r, mysql::Pool>) -> Response<'a> {
	println!("{:?}", repo_user);
	build_success_generic("hello".to_string())
}	