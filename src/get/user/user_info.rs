use rocket::State;
use rocket::response::Response;
use crate::common::response::{build_success_json};
use crate::common::user_token::UserToken;

#[get("/userInfo")]
pub fn user_info<'a, 'r>(user_token: UserToken, state: State<'r, mysql::Pool>) -> Response<'a> {		
	let usr_id = user_token.identify().unwrap();
	let pool = state.inner();
	let query = "SELECT img_ref, gitlab_id, gitlab_API_key FROM user WHERE usr_id = (:usr_id) ".to_string();
	let mysql_row = pool.prep_exec(query, params!(usr_id)).expect("id_and_img: error with query").next().expect("id_and_img: No data");
	let (usr_img, gitlab_id, gitlab_api_key): (Option<String>, Option<i32>, Option<String>) = mysql::from_row(mysql_row.expect("id_and_img: err readinng row"));
	let json = json!({
    	"user_id": usr_id,
    	"usr_img": usr_img,
    	"gitlab_id": gitlab_id,
    	"gitlab_api_key": gitlab_api_key
	});
																																				
	build_success_json(json)
}