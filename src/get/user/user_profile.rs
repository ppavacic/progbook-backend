use rocket::State;
use crate::common::response::{build_success_json, build_failure_generic};
use crate::common::user_token::UserToken;
use rocket::http::{Status};
use rocket::response::{Response};


#[get("/user/<subject_id>")]
pub fn user_posts<'r>(user_token: UserToken, subject_id: i32, state: State<'r, mysql::Pool>) -> Response<'static> {
	let stalker_id = user_token.identify().unwrap();
	let pool = state.inner();
	let query = "SELECT post_id, content_txt, create_date 
					FROM post 
				WHERE usr_id = (:subject_id)
				ORDER BY create_date DESC
				LIMIT 40".to_string();
	
	match pool.prep_exec(query, params!(subject_id)) {
		Ok(data) => {
			let mut json: Vec<serde_json::Value> = Vec::new();
			for row in data {
				let (post_id, content_txt, create_date) : (i32, String, mysql::Value) = mysql::from_row(row.expect("Unable to read row"));
				let create_date = create_date.as_sql(false);
				json.push(json!({
					"post_id": post_id,
					"content_txt": content_txt,
					"create_date": create_date
				}));
			}

			let query = "SELECT display_name, personal_quote, img_ref, 
						CASE
							WHEN subject_id IS NOT NULL THEN true
							ELSE false
						END AS following
							FROM user 
							LEFT JOIN (
								SELECT * FROM user_follow 
								WHERE subject_id = (:subject_id) AND stalker_id = (:stalker_id)
							) AS follow_check ON subject_id = usr_id
						WHERE user.usr_id = (:subject_id);".to_string();
			let (subject_name, subject_quote, subject_img, following) : 
				(Option<String>, Option<String>, Option<String>, bool) 
			= mysql::from_row(pool.prep_exec(query, params!(subject_id, stalker_id)).expect("usr_info: wrong query syntax").next().expect("No user").expect("some"));
			return build_success_json(json!({
				"usr_name": subject_name,
				"usr_id": subject_id,
				"usr_posts": json,
				"image": subject_img,
				"usr_quote": subject_quote,
				"following": following
			}));
		},
		Err(err) => {
			println!("get_user_posts: Error with query syntax \n{}\n", err);
			return build_failure_generic("Internal Server Error", Status::InternalServerError);
		}

	}
}