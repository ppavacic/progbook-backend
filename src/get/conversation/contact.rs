use rocket::State;
use rocket::http::{RawStr};
use mysql::Pool;
use rocket::Response;
use crate::common::user_token::UserToken;
use crate::common::response::{build_success_json, build_success_generic};

#[get("/contact/<interl_id>")]
pub fn contact_person<'a, 'r>(user_token: UserToken, interl_id: &RawStr, state: State<'r, Pool>) -> Response<'a> {
	let pool = state.inner();
	let usr_id = user_token.identify().expect("Err");
	let interl_id = interl_id.to_string();

	let query = "
	SELECT conv_id
		FROM conversation 
	WHERE (usr1_id = (:usr_id) AND usr2_id = (:interl_id2)) OR (usr2_id = (:usr_id) AND usr1_id = (:interl_id2));";

	let interl_id2 = interl_id.to_owned();
	match pool.prep_exec(query, params!(usr_id, interl_id2)).expect("Error with query syntax")
		.next() {
			Some(row) => {
				let conv_id : (i32) = 
					mysql::from_row(row.expect("Error transforming row to data"));
				let json = json!({
					"exists": true,
					"conv_id": conv_id,
				});
				build_success_json(json)
			},
			None => {
				let query = "SELECT display_name, img_ref FROM user WHERE usr_id = (:interl_id);";
				let interl_id2 = interl_id.to_owned();
				match pool.prep_exec(query, params!(interl_id)).expect("Errpr with query syntax").next() {
					Some(row) => {
						let (interl_name, interl_img) : (String, Option<String>) = mysql::from_row(row.expect("Database error"));
						let json = json!({
							"exists": false,
							"interl_name": interl_name,
							"interl_id": interl_id2,
							"interl_img": interl_img
						});
						return build_success_json(json);
					},
					None => {
						return build_success_generic("User does not exist".to_string());
					}
				}

			}
		}
	
	
}