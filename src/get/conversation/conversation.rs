use rocket::http::{RawStr, Status};
use rocket::{Response};
use crate::common::{response};
use crate::common::user_token::UserToken;
use rocket::State;
use serde_json::Value;

#[get("/conversation/<conv_id>")]
pub fn messages_list<'a, 'r>(user_token: UserToken, conv_id: &'a RawStr, state: State<'r, mysql::Pool>) -> Response<'a> {
	let pool = state.inner();
	let conv_id = conv_id.as_str();
	let usr_id = user_token.identify().expect("User unauthorized");


	let mut json_messages: Vec<Value> = Vec::new();

	//authorization, user info
	let query = "
	SELECT title, authorization, interl_id, display_name AS interl_name, img_ref FROM (
		SELECT title, 
		CASE
			WHEN usr1_id = (:usr_id) OR usr2_id = (:usr_id) THEN true
			ELSE false
		END AS authorization,
		CASE 
			WHEN usr1_id = (:usr_id) THEN usr2_id
			ELSE usr1_id
		END AS interl_id
		FROM user_conversation NATURAL JOIN conversation 
		WHERE conv_id = (:conv_id)
	) AS becauseCase 
	JOIN user ON usr_id = interl_id".to_string();

	let mut interl_name: Option<String> = None;
	let mut interl_img: Option<String> = None;
	let mut interl_id: i32 = 0;
	match pool.prep_exec(query, params!{usr_id, conv_id})
	.expect("messages_list: Something wrong with input query string").next() {
		Some(data) => {
			let (_conv_title, auth, usr_id, usr_name, usr_img) : (Option<String>, bool, i32, Option<String>, Option<String>) =
			mysql::from_row(data.expect("Database error"));
			interl_name = usr_name;
			interl_id = usr_id;
			interl_img = usr_img;
			if !auth { return response::build_failure_generic("Unauthorized", Status::Unauthorized) }
		},
		None => {
			println!("empty set");
		}
	}

	let query = "
		SELECT from_id, content_txt, create_date
		FROM message 
			NATURAL JOIN conversation 
		WHERE conv_id = (:conv_id)".to_string();
	


	let mut iterations = 0;
	match pool.prep_exec(query, params!{conv_id}) {
		Ok(response) => {
			for data in response {
				match data {
					Ok(row) => {
						let (from_id, content_txt, create_date): (i32, String, mysql::Value) = mysql::from_row(row);
						let mut create_date = create_date.as_sql(false);
						create_date.remove(0);
						create_date.remove(create_date.len() -1); //removing ' and '
						json_messages.push(json!({
							"from_id": from_id,
							"content_txt": content_txt,
							"create_date": create_date
						}));
						iterations = iterations +1;
					},
					Err(err) => {
						println!("messages_list1,Error: \n{}\n", err);
					}
				}
				
			}
		},
		Err(err) => {
			println!("messages_list2, Error: \n{}\n", err);
		}

	}

	let json = json!({
		"from_name": interl_name,
		"from_id": interl_id,
		"from_img": interl_img,
		"messages": json_messages
	});
	response::build_success_json(json)
}
