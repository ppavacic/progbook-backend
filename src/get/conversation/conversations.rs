use crate::common::response::{build_success_json, build_failure_generic};
use crate::common::user_token::UserToken;
use rocket::{Response};
use rocket::State;
use rocket::http::Status;
use serde_json::Value;

#[get("/conversations")]
pub fn conversations_list<'r>(user_token: UserToken, state: State<'r, mysql::Pool>) -> Response<'static>{
	let pool = state.inner();
	let user_id = user_token.identify().expect("Unknown user, #TODO");
	let query = r#"	SELECT conv_id, conv_title, interlocutor_id, display_name, img_ref FROM
					(
						SELECT conv_id, title AS conv_title,
						CASE 
							WHEN usr1_id = user_conversation.usr_id THEN usr2_id
							ELSE usr1_id
						END AS interlocutor_id
						FROM user_conversation 
							NATURAL JOIN conversation
						WHERE user_conversation.usr_id= (:user_id)
					) a JOIN user ON interlocutor_id = user.usr_id
				"#;

	let mut json: Vec<Value> = Vec::new();
	match pool.prep_exec(query, params!{user_id}) {
		Ok(data) => {
			for item in data { 
				let row = item.expect("Error in mysql");
				let (conv_id, _conv_title, interl_id, interl_name, interl_img): (i32, Option<String>, i32, Option<String>, Option<String>) = mysql::from_row(row);
				json.push(json!({
					"conv_id": conv_id,
					"conv_title": "",
					"conv_descr": "",
					"interl_id": interl_id,
					"interl_name": interl_name,
					"interl_img": interl_img
				}));
			}
			let json = json!(json);
			return build_success_json(json);
		},
		Err(err) => {
			println!("{}", err);
			return build_failure_generic("Internal Server Error", Status::InternalServerError);
		}
	}
}
