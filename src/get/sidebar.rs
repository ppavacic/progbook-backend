use rocket::response::Response;
use crate::common::response::{build_success_json};
use crate::common::user_token::UserToken;

#[get("/repos/favorites")]
pub fn favorites_repos<'a>(user_token: UserToken) -> Response<'a> {		
    println!("Searchging user with id {}", user_token.identify().unwrap());

    let json = json!({
    	"0": {
    		"name": "test-favorites-progbook",
    		"web_url": "https://gitlab.com/ppavacic/progbook",
    		"avatar_url": "https://gitlab.com/uploads/-/system/project/avatar/8782988/Rust_programming_language_black_logo.svg.png"
    	},
	});
																																				
	build_success_json(json)
}

#[get("/repos/popular")]
pub fn popular_repos<'a>(user_token: UserToken) -> Response<'a> {		
    println!("Searchging user with id {}", user_token.identify().unwrap());
    

    let json = json!({
    	"0": {
    		"name": "test-popular-progbook",
    		"web_url": "https://gitlab.com/ppavacic/progbook",
    		"avatar_url": "https://gitlab.com/uploads/-/system/project/avatar/8782988/Rust_programming_language_black_logo.svg.png"
    	},
	});
																																				
	build_success_json(json)
}