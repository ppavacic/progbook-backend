use rocket::State;
use crate::common::user_token::UserToken;
use crate::common::response::{build_success_json};
use rocket::Response;

#[get("/search/<search_query>")]
pub fn users_list<'r>(user_token: UserToken, search_query: String, state: State<'r, mysql::Pool>) -> Response<'static> {
	let usr_id = user_token.identify().expect("No user");
	//let query = "SELECT usr_id, display_name, personal_quote, img_ref FROM user WHERE display_name LIKE :search_query".to_string();
	let query = "
	SELECT DISTINCT usr_id, display_name, personal_quote, img_ref, 
	CASE
		WHEN f_table.subject_id IS NOT NULL THEN true
		ELSE false
	END AS following
		FROM user 
		LEFT JOIN 
		(SELECT subject_id 
			FROM user_follow 
		WHERE stalker_id = (:usr_id)) f_table ON usr_id = subject_id
	WHERE display_name LIKE (:search_query);".to_string();
	let pool = state.inner();
	let mut users: Vec<serde_json::Value> = Vec::new();
	let search_query = "%".to_string() + &search_query + "%";
	for row in pool.prep_exec(query, params!(usr_id, search_query)).expect("users_list: Error with query syntax") {
		let (usr_id, display_name, personal_quote, usr_img, following) : 
		(i32, Option<String>, Option<String>, Option<String>, bool) = 
		mysql::from_row(row.expect("Error reading row"));

		users.push(json!({
			"usr_id": usr_id,
			"usr_name": display_name,
			"usr_quote": personal_quote,
			"usr_img": usr_img,
			"following": following
		}));

	};

	let json = json!(users);

	build_success_json(json)
}