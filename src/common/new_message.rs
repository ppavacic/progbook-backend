pub fn new_message(pool: &mysql::Pool, conv_id: i32, from_id: i32, msg_txt: String) -> Result<String, String> {
	let query = r#"CALL CreateMessage( (:conv_id), (:from_id), (:msg_txt) )"#.to_string();

	let msg_txt_cpy = msg_txt.to_owned();
	match pool.prep_exec(query, params!(conv_id, from_id, msg_txt))
	.expect("new_message_input: Something wrong with input query string").next() {
		Some(_data) => {
			return Ok(msg_txt_cpy);
		},
		None => { return Ok(msg_txt_cpy); }
	}
}