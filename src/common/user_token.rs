use crate::common::helper::create_database_connection;
use std::fmt::{Display, Result, Formatter};
use rocket::response::Response;
use super::response::{build_failure_generic};
use rocket::Outcome;
use rocket::http::Status;
use rocket::request::{self, Request, FromRequest};
use std::string::ToString;
pub struct UserToken(String);

fn is_valid(_key: &str) -> bool {
    true
}

impl UserToken {
	pub fn identify<'a>(&self) -> std::result::Result<i32, Response<'static>> {
		let pool = create_database_connection();
		let query = "SELECT usr_id FROM login WHERE (user_token=(:user_token))";
		match pool.prep_exec(query, params!{"user_token" => self.to_string().as_str()}) {
			Ok(mut data) => {
				match data.next() {
					Some(row) => {
						let usr_id: i32 = mysql::from_row(row.expect("⁉️ identify_user: \t Error unpacking row"));
						return Ok(usr_id);
					},
					None => {
						println!("⚠️ identify_user unknown token\t");
						return Err(build_failure_generic("Invalid token", Status::Unauthorized));
					},
				}
			},
			Err(err) => {
				println!("⁉️ identify_user error with input\t{}", err);
				return Err(build_failure_generic("Unknown Error", Status::InternalServerError));
			}
		}
	}
}

impl Display for UserToken {
	fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "{}", self.0)
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for UserToken {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<UserToken, ()> {
        let keys: Vec<_> = request.headers().get("User-Token").collect();
        if keys.len() != 1 {
            return Outcome::Failure((Status::BadRequest, ()));
        }

        let key = keys[0];
        if !is_valid(keys[0]) {
            return Outcome::Forward(());
        }

        return Outcome::Success(UserToken(key.to_string()));
    }
}