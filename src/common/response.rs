use rocket::response::{Response};
use rocket::http::{ContentType, Status};
use std::io::Cursor;

pub fn build_success_generic<'a>(description: String) -> Response<'a> {
	Response::build()
		.raw_header("Access-Control-Allow-Origin", "*")
		.header(ContentType::HTML)
		.sized_body(Cursor::new(description))
		.finalize()

}

pub fn build_success_json<'a>(json: serde_json::Value) -> Response<'a> {
	Response::build()
		.raw_header("Access-Control-Allow-Origin", "*")
		.header(ContentType::JSON)
		.sized_body(Cursor::new(json.to_string()))
		.finalize()
}


pub fn build_failure_generic<'a>(description: &'a str, status: Status) -> Response<'a> {
	Response::build()
		.raw_header("Access-Control-Allow-Origin", "*")
		.status(status)
		.header(ContentType::HTML)
		.sized_body(Cursor::new(description))
		.finalize()
}