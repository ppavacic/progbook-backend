use sha2::{Sha512, Digest};
use mysql::{Pool, self};
use blob::Blob;



pub fn hash_string(input: String) -> Blob {
	let mut hasher = Sha512::default();
    hasher.input(input);
    Blob::from(hasher.result().as_ref())
}

pub fn create_database_connection() -> Pool {
	Pool::new("mysql://progbook-admin:progbook-admin@localhost:3306/progbook").expect("Can't connect to database")
}
